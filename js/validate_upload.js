class FileUpload {

    constructor(file) {
        this.file = file;
        this.encrypted = false
    }

    getNameFile() {
        return this.file.name
    }

    getSizeFile() {
        return this.file.size
    }

    getTypeFile() {
        return this.file.type
    }

    getEncrypted() {
        return this.encrypted
    }

    readFileInput() {
        const that = this;
        return new Promise((resolve, reject) => {
            let fr = new FileReader();
            fr.onerror = reject;
            fr.onloadend = () => {
                resolve(fr.result)
            };
            fr.readAsText(that.file);
        });
    }

    checkIsPDF() {
        return this.file.type === 'application/pdf'
    }

    checkIsPDFEncrypted(content) {
        this.encrypted = (content.indexOf('/Encrypt') !== -1);
    }

    getVersionPDF(content) {
        if (typeof (content) != "undefined") {
            return content.slice(5, 8)
        }
        return ""
    }
}

(function () {

    /**
     *  Input de Subir Archivos
     */
    let inputFile = document.getElementById('archivos');

    /**
     *  Tabla de archivos
     */
    let tabla_archivo = document.getElementById("tabla_archivos");

    let tabla_archivo_filelist = document.getElementById("tabla_archivos_filelist");

    /**
     * Lista de los archivos cargados
     */
    let includedFiles = []
    let excludedFiles = []

    /**
     * Listener cuando realizo la carga de documentos
     */
    inputFile.addEventListener('change', changeInputFileListener)

    /**
     * Funcion Change Input File Listener
     * @param ev
     * @private
     */
    function changeInputFileListener(ev) {

        cleanInputFilesArray()
        cleanTable()

        const inputFile = ev.target
        let files = inputFile.files
        let promises = []

        if (files.length > 0) {

            showMessageUploadedFiles(files)

            for (let i = 0; i < files.length; i++) {

                let file = new FileUpload(files[i]);

                promises[i] = new Promise((resolve) => {

                    if (file.checkIsPDF()) {

                        file.readFileInput().then((content) => {

                            file.checkIsPDFEncrypted(content)

                            let version = file.getVersionPDF(content)

                            if (file.getEncrypted()) {

                                addIndexToExcludedFiles(i)
                                alertMessageExcludeFile(file)
                                addRowInTable(i, file, version)

                            } else {

                                addRowInTable(i, file, version)
                                addIndexToIncludesFiles(i)

                            }

                            resolve(true)

                        })

                    } else {

                        addIndexToIncludesFiles(i)
                        addRowInTable(i, file, null)

                        resolve(true)

                    }
                })
            }

            Promise.all(promises).then((values) => {
                updateFileList(includedFiles)
                showFiles()
            });
        }
    }

    function cleanInputFilesArray() {
        includedFiles = []
        excludedFiles = []
    }

    function showFiles() {
        let files = document.getElementById("archivos").files
        for (let i = 0; i < files.length; i++) {
            addRowInTableFileList(i, new FileUpload(files[i]))
        }
    }

    function updateFileList(includedFiles) {
        let fileBuffer = new DataTransfer();
        let files = document.getElementById("archivos").files

        for (let i = 0; i < includedFiles.length; i++) {
            for (let j = 0; j < files.length; j++) {
                if (includedFiles[i] === j) {
                    fileBuffer.items.add(files[i]);
                }
            }
        }

        document.getElementById("archivos").files = fileBuffer.files;
    }

    function showMessageUploadedFiles(files) {
        alert("Se han subido " + files.length + " archivos.");
    }

    function addRowInTable(i, file, pdfVersion) {
        tabla_archivo.insertAdjacentHTML("beforeend", constructRow(i, file, pdfVersion))
    }

    function addRowInTableFileList(i, file) {
        tabla_archivo_filelist.insertAdjacentHTML("beforeend", constructRow(i, file))
    }

    function addIndexToIncludesFiles(i) {
        includedFiles.push(i)
    }

    function addIndexToExcludedFiles(i) {
        excludedFiles.push(i)
    }

    function alertMessageExcludeFile(file) {
        if (file.getEncrypted()) {
            alert("El archivo " + file.getNameFile() + " no se puede subir, ya que esta cifrado.")
        }
    }

    function cleanTable() {
        tabla_archivo.innerHTML = ""
        tabla_archivo_filelist.innerHTML = ""
    }

    function constructRow(i, file, pdfVersion) {
        let row = "<tr " + (file.getEncrypted() ? "class='table-danger'" : "") + ">" +
            "<td>" + file.getNameFile() + "</td>" +
            "<td>" + file.getTypeFile() + "</td>" +
            "<td>" + file.getSizeFile() + "</td>" +
            "<td>" + (file.getEncrypted() ? "SI" : "NO") + "</td>";

        if (pdfVersion != null) {
            row += "<td>" + (pdfVersion ? pdfVersion : "--") + "</td>" +
                "</tr>";
        }
        return row
    }

})();

